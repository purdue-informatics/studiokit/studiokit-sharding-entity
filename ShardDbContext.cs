﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity;
using StudioKit.Data.Entity.Utils;
using StudioKit.Sharding.Entity.Interfaces;
using StudioKit.Sharding.Entity.Utils;
using StudioKit.Sharding.Models;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Sharding.Entity;

public class ShardDbContext<TConfiguration, TLicense>
	: DbContext, IShardDbContext<TConfiguration, TLicense>
	where TConfiguration : BaseShardConfiguration, new()
	where TLicense : BaseLicense, new()
{
	private readonly DbConnection _dbConnection;
	protected readonly string NameOrConnectionString;

	public ShardDbContext()
	{
	}

	public ShardDbContext(DbContextOptions options) : base(options)
	{
	}

	public ShardDbContext(DbConnection existingConnection)
	{
		_dbConnection = existingConnection;
	}

	public ShardDbContext(string nameOrConnectionString)
	{
		NameOrConnectionString = nameOrConnectionString;
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		if (!string.IsNullOrWhiteSpace(NameOrConnectionString))
		{
			ShardDbContextUtils.UseSqliteOrSqlServer(optionsBuilder, NameOrConnectionString);
		}
		else if (_dbConnection != null)
		{
			optionsBuilder.UseSqliteOrSqlServer(_dbConnection);
		}
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);

		if (Database.IsSqlite())
		{
			SqliteTimestampConverter.Configure(builder);
		}
	}

	public virtual DbSet<TConfiguration> Configurations { get; set; }

	public virtual DbSet<TLicense> Licenses { get; set; }

	public TConfiguration GetConfiguration()
	{
		return Configurations.OrderByDescending(c => c.Id).FirstOrDefault();
	}

	public async Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default)
	{
		return await Configurations.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
	}

	public TLicense GetLicense()
	{
		return Licenses.OrderByDescending(c => c.Id).FirstOrDefault();
	}

	public async Task<TLicense> GetLicenseAsync(CancellationToken cancellationToken = default)
	{
		return await Licenses.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
	}
}
﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Sharding.Models;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Sharding.Entity.Interfaces;

public interface IShardDbContext<TConfiguration, TLicense>
	where TConfiguration : BaseShardConfiguration, new()
	where TLicense : BaseLicense, new()
{
	DbSet<TConfiguration> Configurations { get; set; }

	DbSet<TLicense> Licenses { get; set; }

	TConfiguration GetConfiguration();

	Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default);

	TLicense GetLicense();

	Task<TLicense> GetLicenseAsync(CancellationToken cancellationToken = default);
}
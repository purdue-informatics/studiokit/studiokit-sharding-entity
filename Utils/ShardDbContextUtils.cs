using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Utils;

namespace StudioKit.Sharding.Entity.Utils;

public static class ShardDbContextUtils
{
	public static DbContextOptionsBuilder UseSqliteOrSqlServer(this DbContextOptionsBuilder optionsBuilder, string nameOrConnectionString)
	{
		if (DbContextUtils.ShouldUseSqlite(nameOrConnectionString))
		{
			optionsBuilder.UseSqlite(
				nameOrConnectionString, o =>
					o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery));
		}
		else
		{
			optionsBuilder.UseSqlServer(
				ShardConnectionStringFactory.GetShardConnectionString(nameOrConnectionString),
				o =>
				{
					o.EnableRetryOnFailure();
					o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery);
				});
			optionsBuilder.AddQueryHints();
		}

		return optionsBuilder;
	}
}
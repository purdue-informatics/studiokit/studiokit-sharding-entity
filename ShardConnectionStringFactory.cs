using Microsoft.Data.SqlClient;
using StudioKit.Sharding.Exceptions;
using StudioKit.Sharding.Extensions;
using System;

namespace StudioKit.Sharding.Entity;

public static class ShardConnectionStringFactory
{
	/// <summary>
	/// Creates a shard SQL connection string based on the given shard key, if possible.
	/// If <paramref name="shardKeyOrConnectionString"></paramref> is already a valid connection string,
	/// then the existing connection string is returned.
	/// </summary>
	/// <param name="shardKeyOrConnectionString">A shard key or connection string.</param>
	/// <returns>
	/// A SQL connection string
	/// </returns>
	/// <exception cref="ArgumentNullException">If <paramref name="shardKeyOrConnectionString"/> is null or whitespace</exception>
	/// <exception cref="ShardException">
	/// If one of the following is true:
	/// <list type="bullet">
	/// <item><paramref name="shardKeyOrConnectionString"/> is <see cref="ShardDomainConstants.RootShardKey"/></item>
	/// <item><paramref name="shardKeyOrConnectionString"/> is <see cref="ShardDomainConstants.InvalidShardKey"/></item>
	/// <item><see cref="ShardManager.DefaultShardMap"/> on <see cref="ShardManager.Instance"/> does not exist</item>
	/// <item>The target shard does not exist in <see cref="ShardManager.DefaultShardMap"/></item>
	/// </list>
	/// </exception>
	public static string GetShardConnectionString(string shardKeyOrConnectionString)
	{
		if (string.IsNullOrWhiteSpace(shardKeyOrConnectionString))
			throw new ArgumentNullException(nameof(shardKeyOrConnectionString));

		if (shardKeyOrConnectionString.Equals(ShardDomainConstants.RootShardKey))
			throw new ShardException("Cannot connect to database. Root shard key.");

		if (shardKeyOrConnectionString.Equals(ShardDomainConstants.InvalidShardKey))
			throw new ShardException("Cannot connect to database. Invalid shard key.");

		// Test if "shardKeyOrConnectionString" is already a valid connection string, and return it
		try
		{
			var connectionBuilder = new SqlConnectionStringBuilder(shardKeyOrConnectionString);
			return connectionBuilder.ConnectionString;
		}
		catch (ArgumentException)
		{
			// ignored - If "shardKeyOrConnectionString" is not a connection string, then proceed
		}

		var shardMap = ShardManager.Instance.DefaultShardMap;
		if (shardMap == null)
			throw new ShardException("DefaultShardMap does not exist");

		shardMap.TryGetMappingForKey(shardKeyOrConnectionString.ToPointMapping(), out var shardMapping);
		if (shardMapping == null)
			throw new ShardException("Shard does not exist in DefaultShardMap");

		var location = shardMapping.Shard.Location;
		return location.GetConnectionString();
	}
}
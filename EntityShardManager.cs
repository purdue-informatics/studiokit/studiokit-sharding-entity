using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Logging;
using StudioKit.Data.Entity.Interfaces;
using StudioKit.Sharding.Entity.Interfaces;
using StudioKit.Sharding.Extensions;
using StudioKit.Sharding.Models;
using StudioKit.Sharding.Utils;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Sharding.Entity;

public class EntityShardManager<TContext, TConfiguration, TLicense> : ShardManager
	where TContext : DbContext, IShardDbContext<TConfiguration, TLicense>, new()
	where TConfiguration : BaseShardConfiguration, new()
	where TLicense : BaseLicense, new()
{
	public EntityShardManager(ILoggerFactory loggerFactory, ILogger<EntityShardManager<TContext, TConfiguration, TLicense>> logger) : base(
		loggerFactory, logger)
	{
	}

	protected string BusinessLogicMigrationsAssemblyName { get; set; } = "BusinessLogic";

	protected string BusinessLogicMigrationsNamespace { get; set; } = "Migrations";

	protected virtual TContext GetDbContext(string connectionString)
	{
		throw new NotImplementedException();
	}

	protected virtual int GetMinCommandTimeout()
	{
		return 300;
	}

	private TContext GetDbContext(ShardLocation location)
	{
		var dbContext = GetDbContext(location.GetConnectionString());
		dbContext.Database.SetCommandTimeout(GetMinCommandTimeout());
		return dbContext;
	}

	protected override async Task<MigrationStatus> MigrateShardDatabaseAsync(ShardLocation location, string targetMigration = null,
		CancellationToken cancellationToken = default)
	{
		var dbContext = GetDbContext(location);

		var currentMigrations = (await dbContext.Database.GetAppliedMigrationsAsync(cancellationToken)).ToList();
		var currentMigration = currentMigrations.LastOrDefault();
		var pendingMigrations = (await dbContext.Database.GetPendingMigrationsAsync(cancellationToken)).ToList();

		if (!string.IsNullOrWhiteSpace(targetMigration) && !currentMigrations.Contains(targetMigration) &&
			!pendingMigrations.Contains(targetMigration))
			throw new ArgumentException($"targetMigration value \"{targetMigration}\" is invalid");

		if (string.IsNullOrWhiteSpace(targetMigration) && !pendingMigrations.Any() ||
			!string.IsNullOrWhiteSpace(targetMigration) && currentMigration == targetMigration)
		{
			OutputInfo("No pending migrations for \"{0}\"", location.Database);
			return new MigrationStatus
			{
				Database = location.Database,
				CurrentMigrations = currentMigrations,
				PendingMigrations = pendingMigrations
			};
		}

		var businessLogicMigrationsAssembly = !string.IsNullOrWhiteSpace(BusinessLogicMigrationsAssemblyName) &&
											!string.IsNullOrWhiteSpace(BusinessLogicMigrationsNamespace)
			? Assembly.Load(BusinessLogicMigrationsAssemblyName)
			: null;

		var migrator = dbContext.Database.GetService<IMigrator>();

		if (businessLogicMigrationsAssembly == null)
		{
			OutputDetailedInfo("Pending migrations \"{0}\"", string.Join(", ", pendingMigrations));
			OutputDetailedInfo("Migrating database \"{0}\"{1}", location.Database,
				string.IsNullOrWhiteSpace(targetMigration)
					? " to latest migration"
					: string.Format($" to TargetMigration \"{targetMigration}\""));
			await migrator.MigrateAsync(targetMigration, cancellationToken);
		}
		else
		{
			// reverting
			if (!string.IsNullOrWhiteSpace(targetMigration) && currentMigrations.Contains(targetMigration))
			{
				var migrationsToRevert = currentMigrations.Skip(currentMigrations.IndexOf(targetMigration)).Reverse().ToList();
				foreach (var migration in migrationsToRevert)
				{
					if (migration == targetMigration) continue;

					var businessLogicMigration = GetBusinessLogicMigration(businessLogicMigrationsAssembly, migration);
					if (businessLogicMigration != null)
					{
						OutputDetailedInfo("Running IBusinessLogicMigration.BeforeDownAsync for \"{0}\"", migration);
						var logger = LoggerFactory.CreateLogger(businessLogicMigration.GetType());
						await businessLogicMigration.BeforeDownAsync(dbContext, logger, cancellationToken);
						OutputDetailedInfo("Finished running IBusinessLogicMigration.BeforeDownAsync for \"{0}\"", migration);
					}

					var previousMigration = migrationsToRevert[migrationsToRevert.IndexOf(migration) + 1];
					OutputDetailedInfo("Reverting migration \"{0}\" to \"{1}\"", migration, previousMigration);
					await migrator.MigrateAsync(previousMigration, cancellationToken);
					OutputDetailedInfo("Reverted migration \"{0}\" to \"{1}\"", migration, previousMigration);
				}
			}
			// applying
			else
			{
				var migrationsToApply = pendingMigrations.Take(string.IsNullOrWhiteSpace(targetMigration)
					? pendingMigrations.Count
					: pendingMigrations.IndexOf(targetMigration) + 1);
				foreach (var migration in migrationsToApply)
				{
					OutputDetailedInfo("Applying migration \"{0}\"", migration);
					await migrator.MigrateAsync(migration, cancellationToken);
					OutputDetailedInfo("Applied migration \"{0}\"", migration);

					var businessLogicMigration = GetBusinessLogicMigration(businessLogicMigrationsAssembly, migration);
					if (businessLogicMigration != null)
					{
						OutputDetailedInfo("Running IBusinessLogicMigration.AfterUpAsync for \"{0}\"", migration);
						var logger = LoggerFactory.CreateLogger(businessLogicMigration.GetType());
						await businessLogicMigration.AfterUpAsync(dbContext, logger, cancellationToken);
						OutputDetailedInfo("Finished running IBusinessLogicMigration.AfterUpAsync for \"{0}\"", migration);
					}
				}
			}
		}

		OutputInfo("Migration complete for \"{0}\"", location.Database);

		currentMigrations = (await dbContext.Database.GetAppliedMigrationsAsync(cancellationToken)).ToList();
		pendingMigrations = (await dbContext.Database.GetPendingMigrationsAsync(cancellationToken)).ToList();
		return new MigrationStatus
		{
			Database = location.Database,
			CurrentMigrations = currentMigrations,
			PendingMigrations = pendingMigrations
		};
	}

	private IBusinessLogicMigration<TContext> GetBusinessLogicMigration(Assembly businessLogicMigrationsAssembly, string migrationKey)
	{
		var migrationName = migrationKey[(migrationKey.IndexOf("_", StringComparison.Ordinal) + 1)..];
		var migrationTypeName = $"{BusinessLogicMigrationsAssemblyName}.{BusinessLogicMigrationsNamespace}.{migrationName}";
		var migrationType = businessLogicMigrationsAssembly.GetType(migrationTypeName);
		if (migrationType != null && typeof(IBusinessLogicMigration<TContext>).IsAssignableFrom(migrationType))
		{
			var migration = (IBusinessLogicMigration<TContext>)Activator.CreateInstance(migrationType);
			if (migration == null) throw new Exception($"Migration class could not be instantiated for type {migrationType}");
			return migration;
		}

		return null;
	}

	protected override async Task<MigrationStatus> GetShardDatabaseMigrationStatusAsync(ShardLocation location,
		CancellationToken cancellationToken = default)
	{
		var dbContext = GetDbContext(location);
		var currentMigrations = (await dbContext.Database.GetAppliedMigrationsAsync(cancellationToken)).ToList();
		var pendingMigrations = (await dbContext.Database.GetPendingMigrationsAsync(cancellationToken)).ToList();
		return new MigrationStatus
		{
			Database = location.Database,
			CurrentMigrations = currentMigrations,
			PendingMigrations = pendingMigrations
		};
	}

	protected override async Task ConfigureShardDatabaseAsync(ShardLocation location, CancellationToken cancellationToken = default)
	{
		var context = GetDbContext(location);
		await AddOrUpdateConfigurationAsync(context, cancellationToken);
		if (IsConsole) await ConsoleAddOrUpdateLicenseAsync(context, cancellationToken);
	}

	private async Task AddOrUpdateConfigurationAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		var configuration = await dbContext.GetConfigurationAsync(cancellationToken) ?? new TConfiguration { Name = CustomerName };
		CustomerName = configuration.Name;

		if (IsConsole)
		{
			ConsoleUtils.WriteDetailedInfo("Current Configuration: ");
			ConsoleUtils.WriteObjectProperties(configuration);
			Console.WriteLine();
			ConsoleUpdateConfiguration(configuration);
		}

		if (configuration.Id == 0)
		{
			dbContext.Configurations.Add(configuration);
		}

		if (dbContext.ChangeTracker.HasChanges() || configuration.Id == 0)
		{
			await dbContext.SaveChangesAsync(cancellationToken);
			if (IsConsole)
			{
				ConsoleUtils.WriteDetailedInfo("Saved Configuration: ");
				ConsoleUtils.WriteObjectProperties(configuration);
			}
		}
		else if (IsConsole)
		{
			ConsoleUtils.WriteDetailedInfo("No changes were made.");
		}

		if (IsConsole) Console.WriteLine();
	}

	private async Task ConsoleAddOrUpdateLicenseAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		var license = await dbContext.GetLicenseAsync(cancellationToken);

		ConsoleUtils.WriteDetailedInfo("License: ");
		ConsoleUtils.WriteObjectProperties(license);
		Console.WriteLine();

		Console.Write("Add a new License? (Y/N or leave blank to skip): ");
		var response = Console.ReadLine();
		if (string.IsNullOrWhiteSpace(response) || !response.ToUpperInvariant().Equals("Y"))
			return;

		license = new TLicense();
		ConsoleUpdateLicense(license);
		dbContext.Licenses.Add(license);
		await dbContext.SaveChangesAsync(cancellationToken);
		ConsoleUtils.WriteDetailedInfo("Saved new License: ");
		ConsoleUtils.WriteObjectProperties(license);
		Console.WriteLine();
	}

	protected virtual void ConsoleUpdateConfiguration(TConfiguration configuration)
	{
		Console.Write("Set the DefaultTimeZoneId (or leave blank for America/Indiana/Indianapolis): ");
		var response = Console.ReadLine();
		configuration.DefaultTimeZoneId = !string.IsNullOrWhiteSpace(response) ? response : "America/Indiana/Indianapolis";

		Console.Write("Set the Redis cache connection string (or leave blank to skip): ");
		response = Console.ReadLine();
		if (!string.IsNullOrWhiteSpace(response))
		{
			configuration.RedisConnectionString = response;
		}
	}

	protected virtual void ConsoleUpdateLicense(TLicense license)
	{
		string response = null;
		while (string.IsNullOrWhiteSpace(response) || !DateTime.TryParse(response, out _))
		{
			Console.Write("Set License Start Date as \"mm/dd/yyyy\": ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && DateTime.TryParse(response, out var startDate))
			{
				license.StartDate = startDate.ToUniversalTime();
			}
			else
			{
				ConsoleUtils.WriteWarning("You must enter a Start Date. Try again.");
			}
		}

		response = null;
		while (string.IsNullOrWhiteSpace(response) || !DateTime.TryParse(response, out _))
		{
			Console.Write("Set License End Date as \"mm/dd/yyyy\": ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response) && DateTime.TryParse(response, out var endDate))
			{
				license.EndDate = endDate.ToUniversalTime();
			}
			else
			{
				ConsoleUtils.WriteWarning("You must enter a End Date. Try again.");
			}
		}
	}
}